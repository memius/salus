package be.holos.salus.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import java.util.Date;

/**
 * @author Dieter D'haeyere on 01/11/2018.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class KeyStats {

    @Id
    @GeneratedValue
    private long id;

    private Date creationDate;

    @JsonProperty("EBITDA")
    private double ebitda;
    private String symbol; // ':'GAIN',
    private String companyName; //Gladstone Investment Corporation',
    private Double dividendRate; //:0.816,
    private Double dividendYield; //':7.8086123,

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss.SSS")
    private Date exDividendDate; //:'2018-11-19 00:00:00.0',

    public long getId() {
        return id;
    }

    public double getEbitda() {
        return ebitda;
    }

    public void setEbitda(final double ebitda) {
        this.ebitda = ebitda;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(final String symbol) {
        this.symbol = symbol;
    }

    public Double getDividendRate() {
        return dividendRate;
    }

    public void setDividendRate(final Double dividendRate) {
        this.dividendRate = dividendRate;
    }

    public Double getDividendYield() {
        return dividendYield;
    }

    public void setDividendYield(final Double dividendYield) {
        this.dividendYield = dividendYield;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(final Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getExDividendDate() {
        return exDividendDate;
    }

    public void setExDividendDate(final Date exDividendDate) {
        this.exDividendDate = exDividendDate;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(final String companyName) {
        this.companyName = companyName;
    }

    //            'EPSSurpriseDollar':None,
    //            'EPSSurprisePercent':27.7778,
    //            'beta':0.640095,
    //            'cash':2733000,
    //                    'consensusEPS':0.18,
    //                    'day200MovingAvg':10.79946,
    //                    'day30ChangePercent':-0.06680597601378813,
    //                    'day50MovingAvg':11.12715,
    //                    'day5ChangePercent':0.03363006923837783,
    //                    'debt':101954000,
    //                    'float':31963022,
    //                    'grossProfit':0,
    //                    'insiderPercent':2.6,
    //                    'institutionPercent':27.4,
    //                    'latestEPS':1.88,
    //                    'latestEPSDate':'2018-03-31',
    //                    'marketcap':342994697,
    //                    'month1ChangePercent':-0.07257026722401203,
    //                    'month3ChangePercent':-0.10311976998669709,
    //                    'month6ChangePercent':-0.016452074391988664,
    //                    'numberOfEstimates':3,
    //                    'peRatioHigh':0,
    //                    'peRatioLow':0,
    //                    'priceToBook':0.89,
    //                    'priceToSales':None,
    //            'profitMargin':None,
    //            'returnOnAssets':14.9,
    //            'returnOnCapital':None,
    //            'returnOnEquity':24.21,
    //            'revenue':16184000,
    //            'revenuePerEmployee':248985,
    //            'revenuePerShare':0,
    //            'sharesOutstanding':32822459,
    //            'shortDate':0,
    //            'shortInterest':0,
    //            'shortRatio':None,
    //            'ttmEPS':0.7500000000000001,
    //            'week52change':11.1621,
    //            'week52high':12.26,
    //            'week52low':9,
    //            'year1ChangePercent':0.10070676960996,
    //            'year2ChangePercent':0.4285714285714284,
    //            'year5ChangePercent':1.36982946298984,
    //            'ytdChangePercent':-0.01626689761644769
}
