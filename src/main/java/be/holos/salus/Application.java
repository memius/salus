package be.holos.salus;

import be.holos.salus.model.KeyStats;
import be.holos.salus.repository.KeyStatsRepository;
import be.holos.salus.service.KeyStatsService;
import be.holos.salus.service.impl.KeyStatsServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import java.util.Date;

/**
 * @author Dieter D'haeyere on 19/10/18.
 */
@SpringBootApplication
public class Application {

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    @Autowired
    KeyStatsService keyStatsService;

    public static void main(final String[] args) {
        SpringApplication.run(Application.class);
    }

    @Bean
    public KeyStatsService keyStatsService(@Autowired final KeyStatsRepository keyStatsRepository) {
        final KeyStatsServiceImpl keyStatsService = new KeyStatsServiceImpl();
        keyStatsService.setKeyStatsRepository(keyStatsRepository);
        return keyStatsService;
    }

    @Bean
    public RestTemplate restTemplate(final RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean
    public CommandLineRunner run(final RestTemplate restTemplate) throws Exception {
        return args -> {
            final KeyStats keyStats = restTemplate.getForObject("https://api.iextrading.com/1.0/stock/gain/stats", KeyStats.class);
            keyStats.setCreationDate(new Date());
            log.info(keyStats.toString());
            keyStatsService.save(keyStats);
        };
    }
}
