package be.holos.salus.service;

import be.holos.salus.model.KeyStats;

/**
 * @author Dieter D'haeyere on 10/11/2018.
 */
public interface KeyStatsService {

    KeyStats save(KeyStats keyStats);
}
