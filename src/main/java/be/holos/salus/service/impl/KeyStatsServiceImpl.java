package be.holos.salus.service.impl;

import be.holos.salus.model.KeyStats;
import be.holos.salus.repository.KeyStatsRepository;
import be.holos.salus.service.KeyStatsService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Dieter D'haeyere on 10/11/2018.
 */
public class KeyStatsServiceImpl implements KeyStatsService {

    @Autowired
    KeyStatsRepository keyStatsRepository;

    @Override
    public KeyStats save(final KeyStats keyStats) {
        return keyStatsRepository.save(keyStats);
    }

    public void setKeyStatsRepository(final KeyStatsRepository keyStatsRepository) {
        this.keyStatsRepository = keyStatsRepository;
    }
}
