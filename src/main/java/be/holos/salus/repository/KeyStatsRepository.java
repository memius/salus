package be.holos.salus.repository;

import be.holos.salus.model.KeyStats;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Dieter D'haeyere on 10/11/2018.
 */
public interface KeyStatsRepository extends CrudRepository<KeyStats, Long> {

}
